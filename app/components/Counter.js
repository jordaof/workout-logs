var React = require('react');

var Counter = React.createClass({
  render: function () {
    return(
      <h2> {this.props.count}h Hours of exercises</h2>
    )
  }
});

module.exports = Counter
