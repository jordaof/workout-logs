var React = require('react'),
    DatePicker = require('react-datepicker'),
    moment = require('moment');
// Apply datepicker style through {css,style}-loader
require('!style!css!react-datepicker/dist/react-datepicker.css');

var Form = React.createClass({
  getInitialState: function() {
    return {
      items: '',
      startDate: moment()
    };
  },
  handleChange: function(date) {
    this.setState({
      startDate: date
    });
  },
  handleSubmit: function(e){
    e.preventDefault();
    var newItem = {
      hours: e.target.hours.value,
      activity: e.target.activity.value,
      activityDate: e.target.activityDate.value
    };
    this.props.onFormSubmit(newItem);
  },
  render: function () {
    return(
      <form className="js-submit" onSubmit={this.handleSubmit}>
        <fieldset>
          <legend>add new activity</legend>
          <input type="number" ref='hours' id="hours" placeholder="00h" />
          <div className="time"><i className="material-icons">alarm</i></div>
          <select id="activity">
            <option value="run">Run</option>
            <option value="Swimming">Swimming</option>
            <option value="bike">Bike</option>
          </select>
          <DatePicker
            dateFormat="DD/MM/YYYY"
            id='activityDate'
            popoverAttachment='bottom right'
            popoverTargetAttachment='top right'
            placeholderText="Select a date"
            selected={this.state.startDate}
            onChange={this.handleChange}
            showYearDropdown
            dateFormatCalendar="MMMM" />
            <div className="calendar"><i className="material-icons">event</i></div>
          <input type="submit" id="addActivity" value="Add" />
        </fieldset>

      </form>
    )
  }
});

module.exports = Form;
