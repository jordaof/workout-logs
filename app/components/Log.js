var React = require('react'),
    LogItem = require('./LogItem');

var Log = React.createClass({
  render: function () {
    return(
      <table className="table-minimal">
        <thead>
          <tr>
            <th>Time</th>
            <th>Activity</th>
            <th>Date</th>
            <th></th>
          </tr>
        </thead>
          <LogItem items={this.props.items} onRemoveItem={this.props.onRemoveItem}/>
      </table>
    )
  }
});

module.exports = Log;
