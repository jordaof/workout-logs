var React = require('react'),
    Form = require('./Form'),
    Counter = require('./Counter'),
    items = JSON.parse(localStorage.getItem("items")) || [],
    Log = require('./Log');
// Apply styles through {css, sass, style}-loader
require("!style!css!resolve-url!sass?sourceMap!../stylesheets/workoutLogs.sass");

var App = React.createClass({
  getInitialState: function(){
    return {
      items: items,
      count: 0
    };
  },
  countHours: function (items) {
    var exercisesHours = 0;

    for (var i = 0; i < items.length; i++) {
      exercisesHours += parseInt(items[i].hours);
    }
    this.setState({
      count: exercisesHours
    });
  },
  addItems: function(newItem){
    var allItems = this.state.items.concat([newItem]);
    this.setState({items: allItems});
    localStorage.setItem("items", JSON.stringify(allItems));
    this.countHours(allItems);
  },
  deleteItem: function(index){
    var allItems = this.state.items;
    allItems.splice(index, 1);
    this.setState({items: allItems});
    localStorage.setItem("items", JSON.stringify(allItems));
    this.countHours(allItems);
  },
  componentWillMount:function () {
    this.countHours(this.state.items);
  },
  render: function () {

    return(
      <div className="wrapper">
          <section>
            <h1>Workout Log</h1>
          </section>
          <section>
            <Form onFormSubmit={this.addItems}/>
          </section>
          <section>
            <Log items={this.state.items} onRemoveItem={this.deleteItem}/>
          </section>
          <section>
            <Counter count={this.state.count} />
          </section>
          <footer></footer>
      </div>
    )
  }
});

module.exports = App;
