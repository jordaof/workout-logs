var React = require('react');

var LogItem = React.createClass({
  handleClick: function (index, e) {
    var allItems = this.props.items;
    e.preventDefault;
    this.props.onRemoveItem(index);
  },
  render: function () {
    if (this.props.items.length !== 0) {
      list = (
      <tbody>
      {this.props.items.map(function(item, i) {
        var boundClick = this.handleClick.bind(this, i);
        return (
          <tr key={i}>
            <td>{item.hours}h</td>
            <td>{item.activity}</td>
            <td>{item.activityDate}</td>
            <td>
              <a href="#" onClick={boundClick} key={i} >
                <i className="material-icons">cancel</i>
              </a>
            </td>
          </tr>
        )
      },this)}
      </tbody>
    );
    } else {
      list = (
        <tbody>
            <tr>
              <td colSpan="4" style={{textAlign: 'center'}}>
              Add some activity
              </td>
            </tr>
          </tbody>
        )
    }
    return list
  }
});

module.exports = LogItem;
